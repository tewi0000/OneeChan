OneeChan
========

OneeChan is a userscript that functions on top of 4chan X and allows you to customize the site with various functions, themes and mascots. Mascots are not obligatory and can be changed or disabled altogether.

Originally developed by [seaweedchan](https://github.com/seaweedchan), this fork adds compatibility with various 4chan X versions and more custom options.

This is a fork of a KevinParnell's fork hosted on github.
https://github.com/KevinParnell/OneeChan

It adds some features that I felt were missing, but very useful. All the new features are listed below.
* Apply button in the settings menu

TODO
----

* Add CI/CD automated release builds, simplify installation

Bulding & Development
---------------------

### Prepare
1. Install grunt globally: `npm install -g grunt-cli` (may require root)
2. Clone the repo: `git clone git@gitlab.com:tewi0000/OneeChan.git`
3. Go to the folder and install dependencies: `cd OneeChan && npm install`

### Build
- Run `grunt` to build.
- Update the version with `grunt patch`, `grunt minor` or `grunt major`.
- Release with `grunt release`.
